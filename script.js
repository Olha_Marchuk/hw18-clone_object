const deepClone = (obj) =>{
    const newObj = {};
    for(let key in obj){
        if(typeof obj[key] === 'object' && obj[key] !== null){
            newObj[key] = deepClone(obj[key])
        }else{
            newObj[key] = obj[key]
        }
    }
    return newObj
}

const userInfo = deepClone({name: 'alkd', 
lastName: 'logjf', 
pets:{
    cats:5, 
    dogs:{
        korgi: 1,
        lablador: 1,
    }
}})

console.log(userInfo)